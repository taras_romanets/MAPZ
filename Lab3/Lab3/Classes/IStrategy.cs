﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    interface IStrategy
    {
        void ApplyPhysics(Player player);

        void CalculatePhysics(Player player);
        
        bool Collide(Player player);
        
        void AddForce(Player player);
    }
}
