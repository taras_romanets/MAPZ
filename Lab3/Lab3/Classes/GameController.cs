﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Lab3.Classes.Decorator;
using Lab3.Classes.Composite;

namespace Lab3.Classes
{
    class GameController
    {
        public Image spriteSheet;
        public List<Ground> grounds;
        public List<Obstacle> cactuses;
        public List<Obstacle> birds;
        //protected Player player;
        public Context player;
        public int dangerSpawn = 10;
        public int countDangerSpawn = 0;


        public ConcreteFactory factory;
        public static GameController instance = new GameController(new ConcreteFactory(), new Dino(new SlowPhysics()));

        public static GameController Instance
        {
            get { return instance; }
        }

        public Context Player
        {
            get { return player; }
            set { player = value; }
        }


        private GameController(ConcreteFactory fact, Player p)
        {
            grounds = new List<Ground>();
            cactuses = new List<Obstacle>();
            birds = new List<Obstacle>();
            player = new Context(p);
            factory = fact;

            spriteSheet = Properties.Resources.sprite;
            GenerateRoad();
        }

        public void MoveMap()
        {
            for (int i = 0; i < grounds.Count; i++)
            {
                grounds[i].transform.position.X -= 4;
                if (grounds[i].transform.position.X + grounds[i].transform.size.Width < 0)
                {
                    grounds.RemoveAt(i);
                    GetNewGround();
                }
            }
            for (int i = 0; i < cactuses.Count; i++)
            {
                cactuses[i].transform.position.X -= 4;
                if (cactuses[i].transform.position.X + cactuses[i].transform.size.Width < 0)
                {
                    cactuses.RemoveAt(i);
                }
            }
            for (int i = 0; i < birds.Count; i++)
            {
                birds[i].transform.position.X -= 4;
                if (birds[i].transform.position.X + birds[i].transform.size.Width < 0)
                {
                    birds.RemoveAt(i);
                }
            }
        }

        public void GetNewGround()
        {
            Ground g = new Ground(new PointF(0 + 100 * 9, 300), new Size(100, 17));
            grounds.Add(g);
            countDangerSpawn++;

            if(countDangerSpawn >= dangerSpawn)
            {
                dangerSpawn = new Random().Next(5, 9);
                countDangerSpawn = 0;
                int obj = new Random().Next(0, 2);


                switch(obj)
                {
                    case 0:
                        Cactus cactus = factory.CreateCactus() as Cactus;
                        cactuses.Add(cactus);
                        break;
                    case 1:
                        Bird bird = factory.CreateBird() as Bird;
                        birds.Add(bird);
                        break;
                }
            }
        }

        public void GenerateRoad()
        {
            for (int i = 0; i < 10; i++)
            {
                Ground g = new Ground(new PointF(0 + 100 * i, 300), new Size(100, 17));
                grounds.Add(g);
                countDangerSpawn++;
            }
        }

        public void DrawObjets(Graphics g)
        {
            for (int i = 0; i < grounds.Count; i++)
            {
                grounds[i].DrawSprite(g);
            }
            for (int i = 0; i < cactuses.Count; i++)
            {
                cactuses[i].CreatePicture(g);
            }
            for (int i = 0; i < birds.Count; i++)
            {
                birds[i].Template(g);
            }
        }

        public void Update(Label label, Timer mainTimer, bool gameOver)
        {
            this.player.Dino.score++;
            label.Text = "Dino - Score: " + player.Dino.score;
            gameOver = player.Dino.physics.Collide(player.Dino);

            if (gameOver)
            {
                mainTimer.Stop();
                gameOver = true;
                this.player.Dino.score = 0;
                label.Text = "Click Up to restart";
            }
            else
            {
                this.player.Dino.physics.ApplyPhysics(player.Dino);
                this.MoveMap();
            }
        }


        public void DrawGame(PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            this.player.Dino.DrawSprite(g);
            this.DrawObjets(g);
        }
    }
}
