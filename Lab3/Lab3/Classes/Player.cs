﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    abstract class Player
    {
        public Phisics physics;
        public int framesCount = 0;
        public int animationCount = 0;
        public int score = 0;

        public Player(PointF position, Size size, IStrategy s)
        {
            physics = new Phisics(position, size, s);
            framesCount = 0;
        }

        public Player() { }

        public abstract void DrawSprite(Graphics g);

        public abstract void DrawNeededSprite(Graphics g, int srcX, int srcY, int width, int height, int delta, float multiplier);

        public abstract void ChangeState(Context ctx, IStrategy s);
    }
}
