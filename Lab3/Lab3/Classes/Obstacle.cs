﻿using System;
using System.Drawing;

namespace Lab3.Classes
{
    abstract class Obstacle
    {
        public Transform transform;

        public Obstacle(PointF position, Size size)
        {
            transform = new Transform(position, size);
        }

        public Obstacle()
        { }

        public void Template(Graphics g)
        {
            DrawSprite();
            CreatePicture(g);
        }
        public abstract void DrawSprite();
        public abstract void CreatePicture(Graphics g);
    }
}
