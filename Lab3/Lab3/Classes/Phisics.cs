﻿using System;
using System.Drawing;

namespace Lab3.Classes
{
    class Phisics
    {
        public Transform transform;
        public IStrategy physicsMethods;

        public float gravity;
        public float a;

        public bool isJumping;
        public bool isCrouching;

        public Phisics(PointF position, Size size, IStrategy strategy)
        {
            transform = new Transform(position, size);
            gravity = 0;
            a = 0.4f;
            isJumping = false;
            isCrouching = false;
            physicsMethods = strategy;
        }

        public void ApplyPhysics(Player p)
        {
            //CalculatePhysics();
            physicsMethods.ApplyPhysics(p);
        }

        public void CalculatePhysics(Player p)
        {
            /*if (transform.position.Y < 250 || isJumping)
            {
                transform.position.Y += gravity;
                gravity += a;
            }
            if (transform.position.Y > 250)
                isJumping = false;*/
            physicsMethods.CalculatePhysics(p);
        }

        public bool Collide(Player p)
        {
            /*for (int i = 0; i < GameController.Instance.cactuses.Count; i++)
            {
                var cactus = GameController.Instance.cactuses[i];
                PointF delta = new PointF();
                delta.X = (transform.position.X + transform.size.Width / 2) - (cactus.transform.position.X + cactus.transform.size.Width / 2);
                delta.Y = (transform.position.Y + transform.size.Height / 2) - (cactus.transform.position.Y + cactus.transform.size.Height / 2);
                if (Math.Abs(delta.X) <= transform.size.Width / 2 + cactus.transform.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= transform.size.Height / 2 + cactus.transform.size.Height / 2)
                    {
                        GameController.Instance.cactuses.RemoveAt(i);
                        return true;
                    }
                }
            }
            for (int i = 0; i < GameController.Instance.birds.Count; i++)
            {
                var bird = GameController.Instance.birds[i];
                PointF delta = new PointF();
                delta.X = (transform.position.X + transform.size.Width / 2) - (bird.transform.position.X + bird.transform.size.Width / 2);
                delta.Y = (transform.position.Y + transform.size.Height / 2) - (bird.transform.position.Y + bird.transform.size.Height / 2);
                if (Math.Abs(delta.X) <= transform.size.Width / 2 + bird.transform.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= transform.size.Height / 2 + bird.transform.size.Height / 2)
                    {
                        GameController.Instance.birds.RemoveAt(i);
                        return true;
                    }
                }
            }
            return false;*/
            return physicsMethods.Collide(p);
        }

        public void AddForce(Player p)
        {
            /*if (!isJumping)
            {
                isJumping = true;
                gravity = -10;
            }*/
            physicsMethods.AddForce(p);
        }
    }
}
