﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    class Cactus : Obstacle
    {
        int srcX;

        public Cactus(PointF point, Size size) : base(point, size)
        {
            DrawSprite();
        }

        public Cactus()
        {

        }

        public override void DrawSprite()
        {
            Random rand = new Random();
            int r = rand.Next(0, 4);

            switch (r)
            {
                case 0:
                    srcX = 754;
                    break;
                case 1:
                    srcX = 804;
                    break;
                case 2:
                    srcX = 704;
                    break;
                case 3:
                    srcX = 654;
                    break;

            }
        }

        public override void CreatePicture(Graphics g)
        {
            g.DrawImage(GameController.Instance.spriteSheet,
                 new Rectangle(new Point((int)transform.position.X, (int)transform.position.Y),
                               new Size(transform.size.Width, transform.size.Height)),
                               srcX, 0, 48, 100, GraphicsUnit.Pixel);
        }

    }
}
