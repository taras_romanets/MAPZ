﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes.Decorator
{
    class Dino : Player
    {
        public Dino(IStrategy s)
        {
            physics = new Phisics(new PointF(100, 249), new Size(50, 50), s);
            score = 0;
            physics.isCrouching = false;
            physics.transform.size.Height = 50;
            physics.transform.position.Y = 250.2f;
        }

        public override void DrawSprite(Graphics g)
        {
            DrawNeededSprite(g, 1518, 0, 79, 91, 88, 1);
        }

        public override void DrawNeededSprite(Graphics g, int srcX, int srcY, int width, int height, int delta, float multiplier)
        {
            framesCount++;
            if (framesCount <= 10)
                animationCount = 0;
            else if (framesCount > 10 && framesCount <= 20)
                animationCount = 1;
            else if (framesCount > 20)
                framesCount = 0;

            g.DrawImage(GameController.Instance.spriteSheet,
                        new Rectangle(new Point((int)physics.transform.position.X, (int)physics.transform.position.Y),
                                      new Size((int)(physics.transform.size.Width * multiplier), physics.transform.size.Height)),
                        srcX + delta * animationCount, srcY, width, height, GraphicsUnit.Pixel);
        }

        public override void ChangeState(Context ctx, IStrategy s)
        {
            ctx.Dino = new CrouchDino(s);
        }
        
    }
}
