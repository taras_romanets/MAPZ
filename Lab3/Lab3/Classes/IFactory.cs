﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Classes
{
    abstract class IFactory
    {
        public abstract Obstacle CreateCactus();
        public abstract Obstacle CreateBird();
    }
}
