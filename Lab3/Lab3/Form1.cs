﻿using Lab3.Classes;
using Lab3.Classes.Decorator;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Lab3
{
    public partial class Form1 : Form
    {
   
        GameController game;
        Timer mainTimer;
        bool gameOver = false;
        int cScore = 0;

        public Form1()
        {
            InitializeComponent();

            this.Width = 700;
            this.Height = 400;
            this.DoubleBuffered = true;
            this.Paint += new PaintEventHandler(DrawGame);
            this.KeyUp += new KeyEventHandler(OnKeyboardUp);
            this.KeyDown += new KeyEventHandler(OnKeyboardDown);
            mainTimer = new Timer();
            mainTimer.Interval = 10;
            mainTimer.Tick += new EventHandler(Update);

            Init();
        }

        private void OnKeyboardDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                    if (!game.player.Dino.physics.isJumping)
                    {
                        cScore = game.player.Dino.score;
                        if(game.player.Dino.score > 1000)
                        {
                            game.player.Dino = new CrouchDino(new FastPhysics());
                        }
                        else
                        {
                            game.player.Dino = new CrouchDino(new SlowPhysics());
                        }
                        game.player.Dino.score = cScore;

                    }
                    break;
                case Keys.Up:
                    if(gameOver)
                    {
                        gameOver = false;
                        Init();
                        break;
                    }
                    if (!game.player.Dino.physics.isCrouching)
                    {
                        game.player.Dino.physics.isCrouching = false;
                        game.player.Dino.physics.AddForce(game.player.Dino);
                    }
                    break;
            }
        }

        private void OnKeyboardUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    if (!game.player.Dino.physics.isCrouching)
                    {
                        game.player.Dino.physics.isCrouching = false;
                        game.player.Dino.physics.AddForce(game.player.Dino);
                    }
                    break;
                case Keys.Down:
                    cScore = game.player.Dino.score;
                    if (game.player.Dino.score > 1000)
                    {
                        game.player.Dino = new Dino(new FastPhysics());
                    }
                    else
                    {
                        game.player.Dino = new Dino(new SlowPhysics());
                    }
                    game.player.Dino.score = cScore;
                    break;
            }
        }

        public void Init()
        {
            game = GameController.Instance;
            mainTimer.Start();
            Invalidate();
        }


        public void Update(object sender, EventArgs e)
        {
            if(game.Player.Dino.score > 1000)
            {
                game.Player.Dino.physics.physicsMethods = new FastPhysics();
            }
            game.player.Dino.score++;
            label1.Text = "Dino - Score: " + game.player.Dino.score;
            gameOver = game.player.Dino.physics.Collide(game.player.Dino);

            if (gameOver)
            {
                mainTimer.Stop();
                gameOver = true;
                game.player.Dino.score = 0;
                label1.Text = "Click Up to restart";
            }
            else
            {
                game.player.Dino.physics.ApplyPhysics(game.player.Dino);
                game.MoveMap();
                Invalidate();
            }
        }

        private void DrawGame(object sender, PaintEventArgs e)
        {
            game.DrawGame(e);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
