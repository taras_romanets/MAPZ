﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    abstract class Animal
    {
        protected string name;
        protected string color = "black";

        public Animal() { }
        public Animal(string n, string c)
        {
            Console.WriteLine("Animal ctr...");
            name = n; color = c;
        }

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Color
        {
            get { return color; }
            set { color = value; }
        }

        public virtual void eat()
        {
            Console.WriteLine($"eating...");
        }

    }
}
