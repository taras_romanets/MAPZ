﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
    struct SRabbit : IJumpable, IRunable
    {
        public string name;
        public string color;

        public SRabbit(string n, string c)
        {
            name = n; color = c;
        }

        public void run()
        {
            Console.WriteLine($"{name} is runnig");
        }

        void IJumpable.jump()
        {
            Console.WriteLine($"{name} is jumping");
        }
    }
}
