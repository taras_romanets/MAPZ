﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Project : IComparer<Project>
    {
        public Project() { }
        public Project(int e)
        {
            ID = e;
        }

        protected int id;
        public int ID 
        { get { return id; } 
          set { id = value; } 
        }

        protected string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int Compare(Project x, Project y)
        {
            if (x.ID == y.ID)
            {
                return 0;
            }
            else if(x.ID > y.ID)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
